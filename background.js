chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id: 'maximizeHotelPics',
    title: 'Maximize hotel pictures',
    contexts: ['image'],
    documentUrlPatterns: [
      '*://*.hilton.com/*',
      '*://*.wyndhamhotels.com/*'
    ],
    // icons: {
    //   "16": "icon-16x16.png",
    //   "32": "icon-32x32.png",
    //   "48": "icon-48x48.png"
    // }
  });
});

chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === 'maximizeHotelPics') {
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      function: maximizeHotelPictures,
      args: [info.srcUrl, info.pageUrl]
    });
  }
});

function maximizeHotelPictures(srcUrl, pageUrl) {
  function maximizeHiltonPictures() {
    const thumbnails = document.querySelectorAll('[data-testid="carousel-image"]');
    const viewportWidth = window.innerWidth;
    const viewportHeight = window.innerHeight;

    thumbnails.forEach(img => {
      let url = new URL(img.src);
      url.searchParams.set('rw', viewportWidth);
      url.searchParams.set('rh', viewportHeight);
      window.open(url.href, '_blank');
    });
  }

  function maximizeWyndhamPictures() {
    const clickedImg = Array.from(document.querySelectorAll('img')).find(img => img.src === srcUrl);
    if (!clickedImg) {
      const issueUrl = new URL('https://gitlab.com/Nowaker/hotel-image-maximizer-chrome-ext/-/issues/new');
      const issueTitle = 'Bug: Wyndham: Clicked image not found';
      const issueDescription = `- **Current URL:** ${pageUrl}\n- **Image Src:** \'${srcUrl}\'\n\n**Additional information:** tell me us much as you can :)`;

      issueUrl.searchParams.set('issue[title]', issueTitle);
      issueUrl.searchParams.set('issue[description]', issueDescription);

      alert('Clicked image not found. You will be redirected to create a bug report on GitLab.');
      window.open(issueUrl.href, '_blank');
      return;
    }
    const propertyCard = clickedImg.closest('.property-card');
    let images;

    if (propertyCard) {
      images = propertyCard.querySelectorAll('.uu-new-photo-gallery .item img');
    } else {
      images = [clickedImg];
    }

    const viewportHeight = window.innerHeight;

    images.forEach(img => {
      const imgAspectRatio = img.naturalWidth / img.naturalHeight;
      const newWidth = Math.round(viewportHeight * imgAspectRatio);
      let url = new URL(img.src);
      // The following line was commented out because it incorrectly encoded the URL parameter:
      // url.searchParams.set('downsize', `${viewportWidth}:*`);
      // The above line resulted in 'downsize=1507%3A*' due to URL encoding issues.
      // : must no be encoded.

      // Semi-manually constructing the URL to avoid encoding issues:
      url.searchParams.delete('downsize');
      url.href = `${url.origin}${url.pathname}?downsize=${newWidth}:*`;
      window.open(url.href, '_blank');
    });
  }

  if (pageUrl.includes('hilton.com')) {
    maximizeHiltonPictures();
  } else if (pageUrl.includes('wyndhamhotels.com')) {
    maximizeWyndhamPictures();
  }
}
