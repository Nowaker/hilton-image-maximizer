# Hotel Image Maximizer

Tired of the low-quality room pictures on hilton.com, stuck at a frustrating 740x506 pixels?

How about wydhamhotels.com with whopping 220x147, a resolution lower than your first playthrough of Quake 1 in 1996?

Did you know Hilton and Wyndham actually store their pictures in 1080p and above, and they cheap out on data transfer costs?

This plugin lets you access these images in their original high quality.
Because it's 2024, not 1996. Almost 30 years after Quake 1.

## Features

- Works on Hilton and Wyndham hotel websites.
- Enhances images to the size of your viewport, and opens them in a new tab.
  - If requested, I can change it to maximum size.

## Installation

1. Clone or download this repository.
2. `chrome://extensions/`
3. `Load unpacked` (top left) and find the project directory.

## User guide

0. Go to [Hilton.com](https://www.hilton.com), find [a property](https://www.hilton.com/en/book/reservation/rooms/?ctyhocn=ABQANQQ&arrivalDate=2025-03-05&departureDate=2025-03-06&room1NumAdults=1), and click `Room details` on a room type that you're interested in.
1. Right click on room image thumbnail.
2. Click "Maximize Hilton pictures". ![](screenshot-1.png)
3. ???
4. PROFIT! ![](screenshot-2.jpg)

## MIT License

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
